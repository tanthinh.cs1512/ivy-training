[Ivy]
179E52844116E0C3 7.5.0 #module
>Proto >Proto Collection #zClass
Ms0 MainPageProcess Big #zClass
Ms0 RD #cInfo
Ms0 #process
Ms0 @AnnotationInP-0n ai ai #zField
Ms0 @TextInP .type .type #zField
Ms0 @TextInP .processKind .processKind #zField
Ms0 @TextInP .xml .xml #zField
Ms0 @TextInP .responsibility .responsibility #zField
Ms0 @UdInit f0 '' #zField
Ms0 @UdProcessEnd f1 '' #zField
Ms0 @UdEvent f3 '' #zField
Ms0 @UdExitEnd f4 '' #zField
Ms0 @PushWFArc f5 '' #zField
Ms0 @GridStep f6 '' #zField
Ms0 @PushWFArc f7 '' #zField
Ms0 @PushWFArc f2 '' #zField
Ms0 @UdMethod f8 '' #zField
Ms0 @GridStep f9 '' #zField
Ms0 @UdProcessEnd f10 '' #zField
Ms0 @PushWFArc f11 '' #zField
Ms0 @PushWFArc f12 '' #zField
Ms0 @UdMethod f13 '' #zField
Ms0 @UdProcessEnd f14 '' #zField
Ms0 @GridStep f16 '' #zField
Ms0 @PushWFArc f17 '' #zField
Ms0 @PushWFArc f15 '' #zField
Ms0 @UdMethod f18 '' #zField
Ms0 @GridStep f21 '' #zField
Ms0 @GridStep f22 '' #zField
Ms0 @UdProcessEnd f25 '' #zField
Ms0 @UdProcessEnd f26 '' #zField
Ms0 @PushWFArc f27 '' #zField
Ms0 @PushWFArc f28 '' #zField
Ms0 @Alternative f20 '' #zField
Ms0 @PushWFArc f24 '' #zField
Ms0 @PushWFArc f30 '' #zField
Ms0 @UdMethod f19 '' #zField
Ms0 @GridStep f29 '' #zField
Ms0 @UdProcessEnd f31 '' #zField
Ms0 @PushWFArc f32 '' #zField
Ms0 @PushWFArc f33 '' #zField
Ms0 @GridStep f35 '' #zField
Ms0 @UdExitEnd f36 '' #zField
Ms0 @Alternative f39 '' #zField
Ms0 @PushWFArc f40 '' #zField
Ms0 @UdProcessEnd f41 '' #zField
Ms0 @PushWFArc f42 '' #zField
Ms0 @GridStep f43 '' #zField
Ms0 @PushWFArc f44 '' #zField
Ms0 @Alternative f45 '' #zField
Ms0 @PushWFArc f46 '' #zField
Ms0 @PushWFArc f47 '' #zField
Ms0 @Alternative f23 '' #zField
Ms0 @PushWFArc f48 '' #zField
Ms0 @PushWFArc f49 '' #zField
Ms0 @PushWFArc f50 '' #zField
Ms0 @UdMethod f51 '' #zField
Ms0 @PushWFArc f34 '' #zField
Ms0 @Alternative f37 '' #zField
Ms0 @PushWFArc f52 '' #zField
Ms0 @PushWFArc f38 '' #zField
Ms0 @GridStep f53 '' #zField
Ms0 @PushWFArc f54 '' #zField
Ms0 @Alternative f55 '' #zField
Ms0 @PushWFArc f56 '' #zField
Ms0 @PushWFArc f57 '' #zField
Ms0 @PushWFArc f58 '' #zField
>Proto Ms0 Ms0 MainPageProcess #zField
Ms0 f0 guid 179E528443C8C3BC #txt
Ms0 f0 method start(practive.Data) #txt
Ms0 f0 inParameterDecl '<practive.Data data> param;' #txt
Ms0 f0 inParameterMapAction 'out.creator=param.data.creator;
out.employee=param.data.employee;
out.employees=param.data.employees;
out.headerDialog=param.data.headerDialog;
out.indexEmployee=param.data.indexEmployee;
out.isProjectManager=param.data.isProjectManager;
out.isSubmit=param.data.isSubmit;
out.isUpdateDialog=param.data.isUpdateDialog;
' #txt
Ms0 f0 outParameterDecl '<practice.Employee employee,List<practice.Employee> employees,practice.Creator creator,String headerDialog,Boolean isUpdateDialog,Boolean isSubmit,Boolean isProjectManager> result;' #txt
Ms0 f0 outParameterMapAction 'result.employee=in.employee;
result.employees=in.employees;
result.creator=in.creator;
result.headerDialog=in.headerDialog;
result.isUpdateDialog=in.isUpdateDialog;
result.isSubmit=in.isSubmit;
result.isProjectManager=in.isProjectManager;
' #txt
Ms0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(Data)</name>
    </language>
</elementInfo>
' #txt
Ms0 f0 84 52 24 24 -15 15 #rect
Ms0 f0 @|UdInitIcon #fIcon
Ms0 f1 371 51 26 26 0 12 #rect
Ms0 f1 @|UdProcessEndIcon #fIcon
Ms0 f3 guid 179E5284447A8AFB #txt
Ms0 f3 actionTable 'out=in;
' #txt
Ms0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ms0 f3 83 779 26 26 -14 15 #rect
Ms0 f3 @|UdEventIcon #fIcon
Ms0 f4 219 779 26 26 0 12 #rect
Ms0 f4 @|UdExitEndIcon #fIcon
Ms0 f5 109 792 219 792 #arcP
Ms0 f6 actionTable 'out=in;
' #txt
Ms0 f6 actionCode 'import app.EmployeeApp;

ivy.log.info(in.isProjectManager);
if (!in.isProjectManager) {
	EmployeeApp employeeApp = new EmployeeApp();
	employeeApp.init();
	out.employees = employeeApp.getEmployees();
	out.creator = employeeApp.getCreator();
} else {
	out = in;
}' #txt
Ms0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getEmployees()</name>
    </language>
</elementInfo>
' #txt
Ms0 f6 168 48 112 32 -41 -8 #rect
Ms0 f6 @|StepIcon #fIcon
Ms0 f7 108 64 168 64 #arcP
Ms0 f2 280 64 371 64 #arcP
Ms0 f8 guid 179E5643D49E9BF9 #txt
Ms0 f8 method deleteEmployee(practice.Employee) #txt
Ms0 f8 inParameterDecl '<practice.Employee employee> param;' #txt
Ms0 f8 inParameterMapAction 'out.employee=param.employee;
' #txt
Ms0 f8 outParameterDecl '<> result;' #txt
Ms0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>deleteEmployee()</name>
    </language>
</elementInfo>
' #txt
Ms0 f8 83 131 26 26 -23 15 #rect
Ms0 f8 @|UdMethodIcon #fIcon
Ms0 f9 actionTable 'out=in;
' #txt
Ms0 f9 actionCode 'import javax.faces.application.FacesMessage;
import org.primefaces.context.RequestContext;
import javax.faces.context.FacesContext;

//remove Employee from List of employee
in.employees.remove(in.employee);
in.employee = null;

//Set message if update success
FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ivy.cms.co("/i18n/successDeleteEmployee"), ""));

//Update partial process
RequestContext.getCurrentInstance().update("formTable");
RequestContext.getCurrentInstance().update("messages");
RequestContext.getCurrentInstance().execute("PF(''theTable'').clearFilters()");' #txt
Ms0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>deleteEmployee</name>
    </language>
</elementInfo>
' #txt
Ms0 f9 168 124 112 40 -41 -8 #rect
Ms0 f9 @|StepIcon #fIcon
Ms0 f10 371 131 26 26 0 12 #rect
Ms0 f10 @|UdProcessEndIcon #fIcon
Ms0 f11 109 144 168 144 #arcP
Ms0 f12 280 144 371 144 #arcP
Ms0 f13 guid 179E5890302AE1FC #txt
Ms0 f13 method preEdit(practice.Employee,Integer) #txt
Ms0 f13 inParameterDecl '<practice.Employee empl,Integer index> param;' #txt
Ms0 f13 inParameterMapAction 'out.employee=param.empl;
out.indexEmployee=param.index;
' #txt
Ms0 f13 outParameterDecl '<> result;' #txt
Ms0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>preEdit()</name>
    </language>
</elementInfo>
' #txt
Ms0 f13 83 227 26 26 -23 15 #rect
Ms0 f13 @|UdMethodIcon #fIcon
Ms0 f14 371 227 26 26 0 12 #rect
Ms0 f14 @|UdProcessEndIcon #fIcon
Ms0 f16 actionTable 'out=in;
out.employee=in.employees.get(in.indexEmployee);
' #txt
Ms0 f16 actionCode 'import practice.Employee;
Employee newEdit = new Employee();
newEdit.setId(in.employee.getId());
newEdit.setAge(in.employee.getAge());
newEdit.setName(in.employee.getName());
newEdit.setTeam(in.employee.getTeam());
out.headerDialog = ivy.cms.co("/i18n/editEmployeeHeaderTitle");
out.isUpdateDialog = true;
out.employee = newEdit;
out.employees = in.employees;' #txt
Ms0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>initValue&#13;
ToEditDialog</name>
    </language>
</elementInfo>
' #txt
Ms0 f16 168 218 112 44 -33 -16 #rect
Ms0 f16 @|StepIcon #fIcon
Ms0 f17 109 240 168 240 #arcP
Ms0 f15 280 240 371 240 #arcP
Ms0 f18 guid 179E5B83F56A5369 #txt
Ms0 f18 method saveData(practice.Employee,Boolean) #txt
Ms0 f18 inParameterDecl '<practice.Employee employee,Boolean isUpdateDialog> param;' #txt
Ms0 f18 inParameterMapAction 'out.employee=param.employee;
out.isUpdateDialog=param.isUpdateDialog;
' #txt
Ms0 f18 outParameterDecl '<> result;' #txt
Ms0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>saveData()</name>
    </language>
</elementInfo>
' #txt
Ms0 f18 83 443 26 26 -23 15 #rect
Ms0 f18 @|UdMethodIcon #fIcon
Ms0 f21 actionTable 'out=in;
' #txt
Ms0 f21 actionCode 'import practice.Employee;
import javax.faces.application.FacesMessage;
import org.primefaces.context.RequestContext;
import javax.faces.context.FacesContext;
import Employee;

//edit Employee from List of employee
Boolean isExist = false;
for(int i = 0; i < in.employees.size(); i++) {
	if (in.employees.get(i).getName() == in.employee.getName()) {
		out.employees.set(i, in.employee);
		isExist = true;
		break;
	}
}

if (isExist) {
	//Set message if update success
	FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ivy.cms.co("/i18n/successUpdateEmployee"), null));
  RequestContext.getCurrentInstance().update("formTable");
 	RequestContext.getCurrentInstance().execute("PF(''theTable'').clearFilters()");
} else {
	FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ivy.cms.co("/i18n/errorUpdateEmployee"), null));
}

//Update partial process
RequestContext.getCurrentInstance().update("messages");' #txt
Ms0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>editEmployee</name>
    </language>
</elementInfo>
' #txt
Ms0 f21 524 496 120 48 -35 -8 #rect
Ms0 f21 @|StepIcon #fIcon
Ms0 f22 actionTable 'out=in;
' #txt
Ms0 f22 actionCode 'import practice.Employee;
import javax.faces.application.FacesMessage;
import org.primefaces.context.RequestContext;
import javax.faces.context.FacesContext;
import Employee;

//add Employee from List of employee
in.employee.id = in.employees.size() > 0 ? in.employees.size() : 0;
in.employees.add(in.employee);

//Set message if update success
FacesContext.getCurrentInstance().
              addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ivy.cms.co("/i18n/successAddEmployee"), null));

//Update partial process
RequestContext.getCurrentInstance().update("formTable");
RequestContext.getCurrentInstance().execute("PF(''theTable'').clearFilters()");
RequestContext.getCurrentInstance().update("messages");' #txt
Ms0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>addEmployee</name>
    </language>
</elementInfo>
' #txt
Ms0 f22 524 432 120 48 -35 -8 #rect
Ms0 f22 @|StepIcon #fIcon
Ms0 f25 691 507 26 26 0 12 #rect
Ms0 f25 @|UdProcessEndIcon #fIcon
Ms0 f26 691 443 26 26 0 12 #rect
Ms0 f26 @|UdProcessEndIcon #fIcon
Ms0 f27 644 456 691 456 #arcP
Ms0 f28 644 520 691 520 #arcP
Ms0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isUpdateDialog ?</name>
    </language>
</elementInfo>
' #txt
Ms0 f20 416 440 32 32 -35 -35 #rect
Ms0 f20 @|AlternativeIcon #fIcon
Ms0 f24 expr in #txt
Ms0 f24 outCond 'in.isUpdateDialog == false' #txt
Ms0 f24 448 456 524 456 #arcP
Ms0 f30 expr in #txt
Ms0 f30 432 472 524 520 #arcP
Ms0 f30 1 432 520 #addKink
Ms0 f30 1 0.3037990578804951 0 0 #arcLabel
Ms0 f19 guid 179E967A7F07E362 #txt
Ms0 f19 method preAdd() #txt
Ms0 f19 inParameterDecl '<> param;' #txt
Ms0 f19 inParameterMapAction 'out.employee=null;
out.isUpdateDialog=false;
' #txt
Ms0 f19 outParameterDecl '<> result;' #txt
Ms0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>preAdd()</name>
    </language>
</elementInfo>
' #txt
Ms0 f19 83 323 26 26 -23 15 #rect
Ms0 f19 @|UdMethodIcon #fIcon
Ms0 f29 actionTable 'out=in;
' #txt
Ms0 f29 actionCode 'out.employee = null;
out.headerDialog = ivy.cms.co("/i18n/newEmployeeHeaderTitle");
out.isUpdateDialog = false;' #txt
Ms0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>initValue&#13;
ToAddDialog</name>
    </language>
</elementInfo>
' #txt
Ms0 f29 168 314 112 44 -33 -16 #rect
Ms0 f29 @|StepIcon #fIcon
Ms0 f31 371 323 26 26 0 12 #rect
Ms0 f31 @|UdProcessEndIcon #fIcon
Ms0 f32 280 336 371 336 #arcP
Ms0 f33 109 336 168 336 #arcP
Ms0 f35 actionTable 'out=in;
' #txt
Ms0 f35 actionCode 'import practice.Employee;
import javax.faces.application.FacesMessage;
import org.primefaces.context.RequestContext;
import javax.faces.context.FacesContext;

out.isSubmit = true;

//check missing mandatory in Creator.`
if (in.creator.getName().isEmpty() || in.creator.getDate() == null || in.creator.comment.isEmpty()) {
	FacesContext.getCurrentInstance().
		addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ivy.cms.co("/i18n/errorMissingMandatory"), null));
  RequestContext.getCurrentInstance().update("messages");
  out.isSubmit = false;
}


' #txt
Ms0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>missing mandatory</name>
    </language>
</elementInfo>
' #txt
Ms0 f35 152 594 112 44 -49 -8 #rect
Ms0 f35 @|StepIcon #fIcon
Ms0 f36 947 603 26 26 0 12 #rect
Ms0 f36 @|UdExitEndIcon #fIcon
Ms0 f39 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isSubmit ?</name>
    </language>
</elementInfo>
' #txt
Ms0 f39 352 600 32 32 -23 -32 #rect
Ms0 f39 @|AlternativeIcon #fIcon
Ms0 f40 264 616 352 616 #arcP
Ms0 f41 643 667 26 26 0 12 #rect
Ms0 f41 @|UdProcessEndIcon #fIcon
Ms0 f42 expr in #txt
Ms0 f42 368 632 643 680 #arcP
Ms0 f42 1 368 680 #addKink
Ms0 f42 1 0.2728986379677914 0 0 #arcLabel
Ms0 f43 actionTable 'out=in;
' #txt
Ms0 f43 actionCode 'import practice.Employee;
import javax.faces.application.FacesMessage;
import org.primefaces.context.RequestContext;
import javax.faces.context.FacesContext;

out.isSubmit = true;

//check at least one employee in the table.
if (in.employees.size() == 0) {
	FacesContext.getCurrentInstance().
		addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ivy.cms.co("/i18n/errorAtLeastTable"), null));
  RequestContext.getCurrentInstance().update("messages");
  out.isSubmit = false;
}


' #txt
Ms0 f43 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>at least an employee</name>
    </language>
</elementInfo>
' #txt
Ms0 f43 448 594 112 44 -53 -8 #rect
Ms0 f43 @|StepIcon #fIcon
Ms0 f44 expr in #txt
Ms0 f44 outCond 'in.isSubmit == true' #txt
Ms0 f44 384 616 448 616 #arcP
Ms0 f45 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isSubmit?&#13;
</name>
    </language>
</elementInfo>
' #txt
Ms0 f45 640 600 32 32 -23 -31 #rect
Ms0 f45 @|AlternativeIcon #fIcon
Ms0 f46 560 616 640 616 #arcP
Ms0 f47 expr in #txt
Ms0 f47 656 632 656 667 #arcP
Ms0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isProjectManager ?</name>
    </language>
</elementInfo>
' #txt
Ms0 f23 208 440 32 32 -38 16 #rect
Ms0 f23 @|AlternativeIcon #fIcon
Ms0 f48 109 456 208 456 #arcP
Ms0 f49 expr in #txt
Ms0 f49 outCond 'in.isProjectManager == false' #txt
Ms0 f49 240 456 416 456 #arcP
Ms0 f50 expr in #txt
Ms0 f50 224 440 704 443 #arcP
Ms0 f50 1 224 416 #addKink
Ms0 f50 2 704 416 #addKink
Ms0 f50 1 0.5029296875 0 0 #arcLabel
Ms0 f51 guid 17A086D607C764C9 #txt
Ms0 f51 method submit(Boolean) #txt
Ms0 f51 inParameterDecl '<Boolean isProjectManager> param;' #txt
Ms0 f51 inParameterMapAction 'out.isProjectManager=param.isProjectManager;
' #txt
Ms0 f51 outParameterDecl '<> result;' #txt
Ms0 f51 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>submit(Boolean)</name>
    </language>
</elementInfo>
' #txt
Ms0 f51 83 603 26 26 -43 16 #rect
Ms0 f51 @|UdMethodIcon #fIcon
Ms0 f34 109 616 152 616 #arcP
Ms0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isProjectManager ?</name>
    </language>
</elementInfo>
' #txt
Ms0 f37 792 600 32 32 -43 -32 #rect
Ms0 f37 @|AlternativeIcon #fIcon
Ms0 f52 expr in #txt
Ms0 f52 outCond 'in.isSubmit == true' #txt
Ms0 f52 672 616 792 616 #arcP
Ms0 f38 expr in #txt
Ms0 f38 outCond 'in.isProjectManager == false' #txt
Ms0 f38 824 616 947 616 #arcP
Ms0 f53 actionTable 'out=in;
' #txt
Ms0 f53 actionCode 'import practice.Employee;
import javax.faces.application.FacesMessage;
import org.primefaces.context.RequestContext;
import javax.faces.context.FacesContext;

out.isSubmit = true;

//check missing mandatory in Creator.`
if (in.creator.getName().isEmpty() || in.creator.getDate() == null || in.creator.comment.isEmpty()) {
	FacesContext.getCurrentInstance().
		addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ivy.cms.co("/i18n/errorFeedBackForm"), null));
  RequestContext.getCurrentInstance().update("messages");
  out.isSubmit = false;
}


' #txt
Ms0 f53 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>feedBack</name>
    </language>
</elementInfo>
' #txt
Ms0 f53 752 660 112 40 -24 -8 #rect
Ms0 f53 @|StepIcon #fIcon
Ms0 f54 expr in #txt
Ms0 f54 808 632 808 660 #arcP
Ms0 f55 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>isSubmit?&#13;
</name>
    </language>
</elementInfo>
' #txt
Ms0 f55 792 736 32 32 -25 12 #rect
Ms0 f55 @|AlternativeIcon #fIcon
Ms0 f56 808 700 808 736 #arcP
Ms0 f57 expr in #txt
Ms0 f57 outCond !in.isSubmit #txt
Ms0 f57 792 752 656 693 #arcP
Ms0 f57 1 656 752 #addKink
Ms0 f57 0 0.7406181964176906 0 0 #arcLabel
Ms0 f58 expr in #txt
Ms0 f58 824 752 960 629 #arcP
Ms0 f58 1 960 752 #addKink
Ms0 f58 0 0.9451123453691215 0 0 #arcLabel
>Proto Ms0 .type practive.MainPage.MainPageData #txt
>Proto Ms0 .processKind HTML_DIALOG #txt
>Proto Ms0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <desc>Yes</desc>
    </language>
</elementInfo>
' #txt
>Proto Ms0 -8 -8 16 16 16 26 #rect
>Proto Ms0 '' #fIcon
Ms0 f3 mainOut f5 tail #connect
Ms0 f5 head f4 mainIn #connect
Ms0 f0 mainOut f7 tail #connect
Ms0 f7 head f6 mainIn #connect
Ms0 f6 mainOut f2 tail #connect
Ms0 f2 head f1 mainIn #connect
Ms0 f8 mainOut f11 tail #connect
Ms0 f11 head f9 mainIn #connect
Ms0 f9 mainOut f12 tail #connect
Ms0 f12 head f10 mainIn #connect
Ms0 f13 mainOut f17 tail #connect
Ms0 f17 head f16 mainIn #connect
Ms0 f16 mainOut f15 tail #connect
Ms0 f15 head f14 mainIn #connect
Ms0 f22 mainOut f27 tail #connect
Ms0 f27 head f26 mainIn #connect
Ms0 f21 mainOut f28 tail #connect
Ms0 f28 head f25 mainIn #connect
Ms0 f20 out f24 tail #connect
Ms0 f24 head f22 mainIn #connect
Ms0 f20 out f30 tail #connect
Ms0 f30 head f21 mainIn #connect
Ms0 f29 mainOut f32 tail #connect
Ms0 f32 head f31 mainIn #connect
Ms0 f19 mainOut f33 tail #connect
Ms0 f33 head f29 mainIn #connect
Ms0 f35 mainOut f40 tail #connect
Ms0 f40 head f39 in #connect
Ms0 f42 head f41 mainIn #connect
Ms0 f39 out f44 tail #connect
Ms0 f44 head f43 mainIn #connect
Ms0 f39 out f42 tail #connect
Ms0 f43 mainOut f46 tail #connect
Ms0 f46 head f45 in #connect
Ms0 f47 head f41 mainIn #connect
Ms0 f18 mainOut f48 tail #connect
Ms0 f48 head f23 in #connect
Ms0 f23 out f49 tail #connect
Ms0 f49 head f20 in #connect
Ms0 f23 out f50 tail #connect
Ms0 f50 head f26 mainIn #connect
Ms0 f51 mainOut f34 tail #connect
Ms0 f34 head f35 mainIn #connect
Ms0 f45 out f52 tail #connect
Ms0 f52 head f37 in #connect
Ms0 f45 out f47 tail #connect
Ms0 f37 out f38 tail #connect
Ms0 f38 head f36 mainIn #connect
Ms0 f37 out f54 tail #connect
Ms0 f54 head f53 mainIn #connect
Ms0 f53 mainOut f56 tail #connect
Ms0 f56 head f55 in #connect
Ms0 f55 out f57 tail #connect
Ms0 f57 head f41 mainIn #connect
Ms0 f55 out f58 tail #connect
Ms0 f58 head f36 mainIn #connect

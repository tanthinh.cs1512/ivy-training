[Ivy]
179D99B3938749B7 7.5.0 #module
>Proto >Proto Collection #zClass
Pe0 Practice Big #zClass
Pe0 B #cInfo
Pe0 #process
Pe0 @AnnotationInP-0n ai ai #zField
Pe0 @TextInP .type .type #zField
Pe0 @TextInP .processKind .processKind #zField
Pe0 @TextInP .xml .xml #zField
Pe0 @TextInP .responsibility .responsibility #zField
Pe0 @StartRequest f0 '' #zField
Pe0 @EndTask f1 '' #zField
Pe0 @UserDialog f11 '' #zField
Pe0 @PushWFArc f3 '' #zField
Pe0 @UserDialog f8 '' #zField
Pe0 @StartRequest f5 '' #zField
Pe0 @PushWFArc f7 '' #zField
Pe0 @EndTask f6 '' #zField
Pe0 @PushWFArc f9 '' #zField
Pe0 @TaskSwitchSimple f10 '' #zField
Pe0 @TkArc f13 '' #zField
Pe0 @UserDialog f2 '' #zField
Pe0 @PushWFArc f4 '' #zField
Pe0 @PushWFArc f12 '' #zField
>Proto Pe0 Pe0 Practice #zField
Pe0 f0 outLink Employee.ivp #txt
Pe0 f0 inParamDecl '<> param;' #txt
Pe0 f0 requestEnabled true #txt
Pe0 f0 triggerEnabled false #txt
Pe0 f0 callSignature Employee() #txt
Pe0 f0 caseData businessCase.attach=true #txt
Pe0 f0 showInStartList 1 #txt
Pe0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Employee.ivp</name>
    </language>
</elementInfo>
' #txt
Pe0 f0 @C|.responsibility Everybody #txt
Pe0 f0 81 161 30 30 -21 17 #rect
Pe0 f0 @|StartRequestIcon #fIcon
Pe0 f1 697 161 30 30 0 15 #rect
Pe0 f1 @|EndIcon #fIcon
Pe0 f11 dialogId practive.MainPage #txt
Pe0 f11 startMethod start(practive.Data) #txt
Pe0 f11 requestActionDecl '<practive.Data data> param;' #txt
Pe0 f11 responseMappingAction 'out.creator=result.creator;
out.employee=result.employee;
out.employees=result.employees;
out.headerDialog=result.headerDialog;
out.indexEmployee=null;
out.isProjectManager=result.isProjectManager;
out.isSubmit=result.isSubmit;
out.isUpdateDialog=result.isUpdateDialog;
' #txt
Pe0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>MainPage</name>
    </language>
</elementInfo>
' #txt
Pe0 f11 216 154 112 44 -25 -8 #rect
Pe0 f11 @|UserDialogIcon #fIcon
Pe0 f3 111 176 216 176 #arcP
Pe0 f8 dialogId practive.DialogComponent #txt
Pe0 f8 startMethod start(practive.Data) #txt
Pe0 f8 requestActionDecl '<practive.Data data> param;' #txt
Pe0 f8 requestMappingAction 'param.data=in;
' #txt
Pe0 f8 responseMappingAction 'out=in;
out=result.data;
' #txt
Pe0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>createComponentDialog</name>
    </language>
</elementInfo>
' #txt
Pe0 f8 280 346 144 44 -64 -8 #rect
Pe0 f8 @|UserDialogIcon #fIcon
Pe0 f5 outLink start2.ivp #txt
Pe0 f5 inParamDecl '<> param;' #txt
Pe0 f5 requestEnabled true #txt
Pe0 f5 triggerEnabled false #txt
Pe0 f5 callSignature start2() #txt
Pe0 f5 caseData businessCase.attach=true #txt
Pe0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start2.ivp</name>
    </language>
</elementInfo>
' #txt
Pe0 f5 @C|.responsibility Everybody #txt
Pe0 f5 81 353 30 30 -24 17 #rect
Pe0 f5 @|StartRequestIcon #fIcon
Pe0 f7 424 368 497 368 #arcP
Pe0 f6 497 353 30 30 0 15 #rect
Pe0 f6 @|EndIcon #fIcon
Pe0 f9 111 368 280 368 #arcP
Pe0 f10 actionTable 'out=in1;
out.isProjectManager=true;
' #txt
Pe0 f10 taskData 'TaskA.NAM=Task\: Request Approval From Project Manager
TaskA.ROL=ProjectManager
TaskA.TYPE=0' #txt
Pe0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name></name>
    </language>
</elementInfo>
' #txt
Pe0 f10 388 160 40 32 0 16 #rect
Pe0 f10 @|TaskSwitchSimpleIcon #fIcon
Pe0 f13 328 176 388 176 #arcP
Pe0 f2 dialogId practive.MainPage #txt
Pe0 f2 startMethod start(practive.Data) #txt
Pe0 f2 requestActionDecl '<practive.Data data> param;' #txt
Pe0 f2 requestMappingAction 'param.data.creator=in.creator;
param.data.employee=in.employee;
param.data.employees=in.employees;
param.data.headerDialog=in.headerDialog;
param.data.indexEmployee=in.indexEmployee;
param.data.isProjectManager=in.isProjectManager;
param.data.isSubmit=in.isSubmit;
param.data.isUpdateDialog=in.isUpdateDialog;
' #txt
Pe0 f2 responseMappingAction 'out=in;
out.isProjectManager=true;
' #txt
Pe0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Project Manager</name>
    </language>
</elementInfo>
' #txt
Pe0 f2 488 154 112 44 -42 -8 #rect
Pe0 f2 @|UserDialogIcon #fIcon
Pe0 f4 428 176 488 176 #arcP
Pe0 f12 600 176 697 176 #arcP
>Proto Pe0 .type practive.Data #txt
>Proto Pe0 .processKind NORMAL #txt
>Proto Pe0 0 0 32 24 18 0 #rect
>Proto Pe0 @|BIcon #fIcon
Pe0 f8 mainOut f7 tail #connect
Pe0 f7 head f6 mainIn #connect
Pe0 f0 mainOut f3 tail #connect
Pe0 f3 head f11 mainIn #connect
Pe0 f5 mainOut f9 tail #connect
Pe0 f9 head f8 mainIn #connect
Pe0 f11 mainOut f13 tail #connect
Pe0 f13 head f10 in #connect
Pe0 f10 out f4 tail #connect
Pe0 f4 head f2 mainIn #connect
Pe0 f2 mainOut f12 tail #connect
Pe0 f12 head f1 mainIn #connect

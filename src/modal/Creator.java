package modal;

import java.util.Date;

public class Creator {
	
	private String name;
	
	private Date date;
	
	private String comment;

	public Creator() {
		
	}
	
	public Creator(String name, Date date, String comment) {
		this.name = name;
		this.date = date;
		this.comment = comment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}

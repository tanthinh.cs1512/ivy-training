package app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import practice.Creator;
import practice.Employee;

@ManagedBean
@ViewScoped
public class EmployeeApp {
	
	private Creator creator;
	
	private List<Employee> employees = new ArrayList<>();
	
	public void init() {
		//creator = new Creator();
		//creator.setName("Traning Ivy");
		//creator.setDate(new Date());
//		creator.setComment("Hi Tranining Ivy");
	}
	
	public void addEmployee(Employee employee) {
		this.employees.add(employee);
	}
	
	public void updateEmployee(Employee employee) {
		int i = 0;
		for (Employee empl: this.employees) {
			if (empl.getName().equals(employee.getName())) {
				this.employees.set(i, employee);
			}
			i++;
		}
		this.employees.add(employee);
	}
	
	public void deleteEmployee(Employee employee) {
		this.employees.remove(employee);
	}

	public Creator getCreator() {
		return creator;
	}

	public void setCreator(Creator creator) {
		this.creator = creator;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	
	

}
